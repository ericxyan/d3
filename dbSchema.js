var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// candidates schema
var candidateSchema = new Schema({
    "firstName": String,
    "lastName": String,
    "twitter": String, // twitter account.
    "hashtags": [String], // #tags used by a candidate within 1 month.
    "district": String, // candidate's district.
    "center": [String], // district center point coordinate
    "extent": [String] // district extent coordinates.
});

// collected tweets to be replied.
var replySchema = new Schema({
    "tweetID": Number, // tweet to be replied.
    "candidate": String, // candidate's twitter acount related.
    "replies": [String], // reply list
    "nextReply": Number, // next reply to be used.
    "date": {
        type: Date,
        default: Date.now
    }
});

// db model
var Candidate = mongoose.model('Candidate', candidateSchema);
var Reply = mongoose.model('Reply', replySchema);

module.exports = {
    'Candidate': Candidate,
    'Reply': Reply
};