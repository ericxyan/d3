const BASE_URL = 'https://represent.opennorth.ca'; // represent.opennorth.ca api root url
const RECENT = 30; // Extract #tags from RECENT number of tweeets.
const HANDLE_CANDIDATES = 15; // Search HANDLE_CANDIDATES candidate's tweets per minute.
const PERIOD = 3000; // Request period.
const TESTNUM = 20; // The number of candidates to get from BASE_URL.

var request = require('request');
var forEach = require('async-foreach').forEach;
var Promise = require('es6-promise').Promise;
var Twit = require('twit');
var mongoose = require('mongoose');
var db = require('./dbSchema');

// config twitter authentication.
var T = new Twit({
    "consumer_secret": "VIUQQfUnHCnm6qfNhaoWrBq6u9ZMt69u2sWSs0TPMqB0JSZi43",
    "consumer_key": "FvZlW8g6WXj0o9HPkSmHGcqjj",
    "access_token": "3471550887-jKDi6g4wRxPEv8t3Y5wi1xYgUnGMr0R5UhGCJNg",
    "access_token_secret": "MpJO5geImldTAKJZm4YZfjlbXNrMXCx0UVOHZFjHzPeXr"
});


// db model
var Candidate = db.Candidate;
var Reply = db.Reply;
var total = [];
// extract twitter @username
var extractAccount = function(url) {
    if (typeof(url) == "string") {
        var a = url.split('/'); //"twitter": "https://twitter.com/JackTrovato",
        return a[a.length - 1]; // JackTrovato  
    } else {
        return '';
    }
};

// connect to local mongoDB server
var connectDB = function() {
    mongoose.connect('mongodb://localhost/local');
    console.log('db ready!');
};

// clean collections
var cleanCollections = function(model) {
    model.remove({}, function(error) {
        console.log('collection removed!');
    });
};

// extract #tag from twitter account.
var getTweets = function(username, num) {
    return new Promise(function(resolve, reject) {
        var params = {
            screen_name: username,
            count: num
        };

        T.get('statuses/user_timeline', params, function(err, data, res) {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
};


// check if a candidate has existed in our db.
var isExist = function(fn, ln) {
    Candidate.count({
        "firstName": fn,
        "lastName": ln
    }, function(err, rs) {
        if (err) return -1;
        console.log(fn + ' ' + ln + ' ' + rs);
        return rs;
    });
};

// extract hashtags from tweet
var extractHashtags = function(text) {
    var hashtags = [];
    if (typeof(text) == 'string') {
        // split into words
        var a = text.split(' '); // delimiter whitespace
        a.forEach(function(e, i, a) {
            // filter out words start with #
            if (e.charAt(0) == '#') {
                // check if the #tag has been extracted.
                if (hashtags.indexOf(e) == -1) hashtags.push(e);
            }
        });
    }
    return hashtags;
};

/* get district coordinate */
var getCoordinates = function(boundaryURL) {
    return new Promise(function(resolve, reject) {
        request(BASE_URL + boundaryURL, function(error, res, body) {
            var data = JSON.parse(body);
            if (error) {
                reject(error);
            } else {
                resolve(data);
            }
        });
    });
};

// create a new record in the db.
var createCandidate = function(candidate) {
    // get a candidate's recent 200 tweets, then filter out #tags used in the 200 tweets.
    if (candidate.extra.twitter) {
        // get twitter @username
        var username = extractAccount(candidate.extra.twitter);
        getTweets(username, RECENT)
            .then(function(result) {
                // result: an array of 200 tweet objects.
                console.log('get tweet:' + result.length);

                new Promise(function(resolve, reject) {
                    var hashtags = []; // To store a collection of tags used by a candidate in recent 200 tweets.
                    // loop
                    forEach(result, function(element, index, array) {
                        var text = element.text; // get a tweet's content.
                        var a = text.split(/[^a-zA-Z#\d]/); // split content. 
                        forEach(a, function(e, i, a) {
                            // filter out words start with #
                            if (e.charAt(0) == '#' && hashtags.indexOf(e) == -1) {
                                // check if the #tag has been extracted.
                                console.log(e);
                                hashtags.push(e);
                            }
                        });
                    });
                    resolve(hashtags);
                })
                    .then(function(result) {
                        // result: an array of #tags. 
                        // write to db
                        getCoordinates(candidate.related.boundary_url)
                            .then(function(boundary) {
                                console.log('get coordinate succeed!');
                                new Candidate({
                                    "firstName": candidate.first_name,
                                    "lastName": candidate.last_name,
                                    "twitter": username, // twitter account.
                                    "hashtags": result, // #tags used by a candidate within 1 month.
                                    "district": candidate.district_name, // candidate's district.
                                    "center": boundary.centroid.coordinates, // district center point coordinate
                                    "extent": boundary.extent // district extent coordinates.
                                })
                                    .save(function(err, docs) {
                                        if (err) {
                                            console.log("Write candidate to db failed!");
                                        } else {
                                            console.log("Write candidate to db succeed!");
                                        }
                                    });
                            });
                    }, function(error) {
                        console.log('Get boundary failed: ' + error);
                    });
            }, function(err) {
                console.log(err);
            });
    } else {
        console.log(candidate.first_name + ' ' + candidate.last_name + ' does not have twitter.');
    }

};

// extract candidate's info and store into db.
var buildCandidates = function(dataArray) {
    // create candidate records to be saved to db.
    dataArray.forEach(function(element, index, array) {
        var fn = element.first_name,
            ln = element.last_name;

        // check if candidate has existed.
        Candidate.count({
            "firstName": fn,
            "lastName": ln
        }, function(err, rs) {
            if (err) {
                console.log('findOne() error!');
            } else {
                if (rs == 0) {
                    // if does not exist in db, create a new one.
                    createCandidate(element);
                } else {
                    // if existed.
                    console.log(fn + ' ' + ln + ' has existed!');
                }
            }
        });
    });
};

var aggregateDistricts = function(){
    Candidate.aggregate([
        {
            "$group": {
                "_id": "$district",
                "tcount": {"$sum": 1},
                "ttags": {"$push": "$tags"}
            }
        },
        {"$unwind": "$ttags"},
        {"$unwind": "$ttags"},
        {
            "$group": {
                "_id": "$_id",
                "tcount": {"$first": "tcount"},
                "tags": {"$addToSet": "$ttags"}
            }
        },
        {
            "$project": {
                "_id": 0,
                "district": "$_id",
                "count": "$tcount",
                "tags": "$ttags"
            }
        }
    ]);
};

module.exports = function(app) {
    // api
    app.post('/api/candidates', function(req, res, next) {
        Candidate.find({
            'firstName': req.body.firstName,
            'lastName': req.body.lastName
        })
            .exec(function(err, docs) {
                if (err) {
                    res.status(500).send('Something broke');
                }
                res.json(docs);
            });
    });

    return {
        // get candidate list from represent.opennorth.ca API and build up local db.
        'init': function() {
            cleanCollections(Candidate);
            // start a sequence
            new Promise(function(resolve, reject) {
                // get the first 1000 candidates
                var limit = "/candidates/?offset=0&limit=" + TESTNUM;
                request(BASE_URL + limit, function(error, res, body) {
                    var data = JSON.parse(body);
                    if (error) {
                        reject('resolve error!');
                    } else {
                        // store 0-1000 candidates to an array
                        total = total.concat(data.objects);
                        console.log('add ' + limit + ' succeed!');
                        // pass to next
                        resolve(data.meta.next);
                    }
                });
            })
                .then(function(result) {
                    // get the next 1000 candidates
                    request(BASE_URL + result, function(error, res, body) {
                        if (error) {
                            console.log('get ' + result + ' error!');
                        } else {
                            var data = JSON.parse(body);
                            // store the next 1000 to the array.
                            total = total.concat(data.objects);
                            console.log("add " + result + " succeed!");
                            console.log("Total: " + data.meta.total_count);
                            console.log("Added: " + total.length);
                            // save 0-2000 candidates info into local db.
                            // 20 call/min, 300 call/15 min.
                            var part = [];
                            var counter = 0;
                            var timerId = setInterval(function() {
                                // get 20 candidates and remove them from the total list.
                                part = total.splice(0, HANDLE_CANDIDATES);
                                // stop interval after extracted all candidates.
                                if (part.length == 0) {
                                    clearInterval(timerId);
                                    console.log('Interval stopped!');
                                } else {
                                    buildCandidates(part);
                                }
                                counter++;
                                console.log('Extracted: ' + part.length);
                                console.log('Timer: ' + counter + ' times.');
                            }, PERIOD);
                        }
                    })
                }, function(err) {
                    console.log(err);
                });
        }
    };
};