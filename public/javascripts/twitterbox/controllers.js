angular.module('myControllers', ['myServices', 'ngSanitize'])
    .controller('twitterBox', function($scope, $http, $filter, $log, $q, socket) {
        $scope.title = "Twitter Box";
        $scope.tweets = [];
        var districtCenter = [
            "-80.7450557833109",
            "43.106486406754236"
        ]; // Oxford
        /**
         * formate a date
         * @param  {string} date selected from date picker.
         * @return {string}      date in 'YYYY-MM-dd' format.
         */
        var formatDate = function(date) {
            return $filter('date')(date, 'yyyy-MM-dd'); // change date to 'YYY-MM-dd'.
        };

        // filter out tweets in required zone. e.g. Oxford: ["-81.13220345190756", "42.82398376366954", "-80.41386569197704", "43.353337404868675"]
        var isLocal = {
            'square': function(tweet, zone) {
                // check if coordinates available, e.g. coordinates: {"type":"Point","coordinates":[120.99871598,14.50890569]}
                if (!tweet.coordinates) {
                    // tweet's coordinate
                    var lat = tweet.coordinates.coordinates[0];
                    var lng = tweet.coordinates.coordinates[1];
                    // zone boundaries
                    var left = zone[0], // -81.13220345190756
                        bottom = zone[1], // 42.82398376366954
                        right = zone[2], // -80.41386569197704
                        top = zone[3]; // 43.353337404868675
                    // check if the tweet within the required zone
                    if (left < lng && lng < right && bottom < lat && lat < top) {
                        return true;
                    } else {
                        return false;
                    }
                }
            },
            'polygon': function(point, vs) {
                // ray-casting algorithm based on
                // http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html

                var x = point[0],
                    y = point[1];

                var inside = false;
                for (var i = 0, j = vs.length - 1; i < vs.length; j = i++) {
                    var xi = vs[i][0],
                        yi = vs[i][1];
                    var xj = vs[j][0],
                        yj = vs[j][1];

                    var intersect = ((yi > y) != (yj > y)) && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
                    if (intersect) inside = !inside;
                }

                return inside;
            }
        }


        /**
         * get recent tweets.
         */
        var searchKeyword = function(params) {
            var deferred = $q.defer();
            // query api.
            $http.post('/api/keyword', params).success(function(res) {
                // get twitter provided html snipped
                res.statuses.forEach(function(tweet, index, tweets) {
                    $http.get('/api/statuses/oembed/' + tweet.id_str).success(function(res) {
                        tweet.html = res.html;
                    });
                });
                deferred.resolve(res.statuses);
            }, function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };


        // Stream listener
        // TODO: filter geo info. 1. get tweets has tweet.place attr.  
        var searchStream = function(params) {
            // socket listens to stream
            socket.on('tweets', function(res) {
                $log.log(res.place);
                $http.get('/api/statuses/oembed/' + res.id_str).success(function(data) {
                    $log.log('get snipped');
                    if (data.place) {
                        var polygon = data.place.bounding_box.coordinates
                        if (isLocal.polygon(districtCenter, polygon)) {
                            res.html = data.html;
                            $scope.tweets.push(res);
                        } else {
                            $log.log(districtCenter);
                            $log.log(polygon);
                        }
                    } else {
                        $log.log(data.place);
                    }
                });
            });
            // request stream
            $http.post('/api/stream/statuses/filter', params).success(function(res) {});
        };

        var forEach = function(array, tags) {
            var deferred = $q.defer();
            if (tags) {
                array.forEach(function(element, index, array) {
                    tags = tags.concat(element.hashtags);
                });
                deferred.resolve(tags);
            } else {
                deferred.reject('tags is not defined');
            }
            return deferred.promise;
        }

        /**
         * Get tweets
         */
        $scope.district = 'Oxford';
        $scope.getTweets = function(district) {
            var tags = [];
            // find all candidates in a district
            $http.get('/api/district/' + district).success(function(res) {
                // collect all hashtags used by candidates in this district
                forEach(res, tags)
                    .then(function(tags) {
                        // build query params.
                        var streamParams = {
                            'track': tags.join(',')
                        };
                        var searchParams = {
                            'keyword': tags.join(' OR '),
                            'count': 5
                            // 'geocode': geo // Toronto: 43.6532260,-79.3831840,30km; home: 43.8134410,-79.3269270,1km
                        };

                        $log.log(tags);
                        // get recent 20 related tweets.
                        searchKeyword(searchParams)
                            .then(function(result) {
                                $scope.tweets = $scope.tweets.concat(result);
                                // start to monitor related tweets.
                                $log.log(streamParams);
                                searchStream(streamParams);

                            }, function(err) {
                                $log.log(err);
                            });
                    }, function(err) {
                        $log.log(err);
                    });
            });
        };

        $scope.getTweets($scope.district);
    });

/**
 * Tweet sample data
 */

/*{
    contributors:

    coordinates: {
        "type": "Point",
        "coordinates": [120.99871598, 14.50890569]
    }

    created_at: Sun Sep 13 04: 41: 46 + 0000 2015

    entities: {
        "hashtags": [{
            "text": "Quark",
            "indices": [0, 6]
        }, {
            "text": "Quarkitd",
            "indices": [10, 19]
        }, {
            "text": "apple",
            "indices": [24, 30]
        }, {
            "text": "samsung",
            "indices": [31, 39]
        }, {
            "text": "asus",
            "indices": [40, 45]
        }, {
            "text": "lenovo",
            "indices": [46, 53]
        }, {
            "text": "hp",
            "indices": [54, 57]
        }, {
            "text": "intel",
            "indices": [58, 64]
        }, {
            "text": "toshiba",
            "indices": [65, 73]
        }, {
            "text": "dell",
            "indices": [74, 79]
        }, {
            "text": "acer",
            "indices": [80, 85]
        }, {
            "text": "microsoft",
            "indices": [86, 96]
        }, {
            "text": "lg",
            "indices": [97, 100]
        }],
        "symbols": [],
        "user_mentions": [],
        "urls": [{
            "url": "https://t.co/7LdCGdWxbf",
            "expanded_url": "https://instagram.com/p/5WLq-TziBF/",
            "display_url": "instagram.com/p/5WLq-TziBF/",
            "indices": [102, 125]
        }]
    }

    favorite_count: 0

    favorited: false

    geo: {
        "type": "Point",
        "coordinates": [14.50890569, 120.99871598]
    }

    id: 642921086158544900

    id_str: 642921086158544896

    in_reply_to_screen_name:

    in_reply_to_status_id:

    in_reply_to_status_id_str:

    in_reply_to_user_id:

    in_reply_to_user_id_str:

    is_quote_status: false

    lang: und

    metadata: {
        "iso_language_code": "und",
        "result_type": "recent"
    }

    place: {
        "id": "004f3727016f9b5c",
        "url": "https://api.twitter.com/1.1/geo/id/004f3727016f9b5c.json",
        "place_type": "city",
        "name": "Paranaque City",
        "full_name": "Paranaque City, National Capital Region",
        "country_code": "PH",
        "country": "Republika ng Pilipinas",
        "contained_within": [],
        "bounding_box": {
            "type": "Polygon",
            "coordinates": [  
                [
                    [120.9741053, 14.4333272],
                    [121.0475014, 14.4333272],
                    [121.0475014, 14.5335013],
                    [120.9741053, 14.5335013]
                ]
            ]
        },
        "attributes": {}
    }

    possibly_sensitive: false

    retweet_count: 0

    retweeted: false

    source: < a href = "http://instagram.com"
    rel = "nofollow" > Instagram < /a>

text: #Quark🌍 . #Quarkitd 📱💻📶 #apple #samsung #asus #lenovo #hp #intel #toshiba #dell #acer #microsoft #lg… https:/ / t.co / 7LdCGdWxbf

    truncated: false

    user: {
        "id": 2789230854,
        "id_str": "2789230854",
        "name": "Quarkit",
        "screen_name": "quarkguarantee",
        "location": "Lausanne,",
        "description": "Quark \nApple،,Samsung,،Microsoft،,ASus،,Lenovo،,Hp،,Intel،,Toshiba،,Dell،,Acer،,LG،,Htc،,Sony،,Motorola،,Huawei،,BlackBerry،,Msi،,Gigabyte،,Alcatel",
        "url": "https://t.co/dcHjOU5GyY",
        "entities": {
            "url": {
                "urls": [{
                    "url": "https://t.co/dcHjOU5GyY",
                    "expanded_url": "https://instagram.com/Quarkit",
                    "display_url": "instagram.com/Quarkit",
                    "indices": [0, 23]
                }]
            },
            "description": {
                "urls": []
            }
        },
        "protected": false,
        "followers_count": 310,
        "friends_count": 2013,
        "listed_count": 5,
        "created_at": "Thu Sep 04 06:39:56 +0000 2014",
        "favourites_count": 0,
        "utc_offset": 16200,
        "time_zone": "Tehran",
        "geo_enabled": true,
        "verified": false,
        "statuses_count": 250,
        "lang": "en",
        "contributors_enabled": false,
        "is_translator": false,
        "is_translation_enabled": false,
        "profile_background_color": "ACDED6",
        "profile_background_image_url": "http://abs.twimg.com/images/themes/theme18/bg.gif",
        "profile_background_image_url_https": "https://abs.twimg.com/images/themes/theme18/bg.gif",
        "profile_background_tile": false,
        "profile_image_url": "http://pbs.twimg.com/profile_images/642570171689889792/uC2WBC4-_normal.jpg",
        "profile_image_url_https": "https://pbs.twimg.com/profile_images/642570171689889792/uC2WBC4-_normal.jpg",
        "profile_banner_url": "https://pbs.twimg.com/profile_banners/2789230854/1441788313",
        "profile_link_color": "038543",
        "profile_sidebar_border_color": "EEEEEE",
        "profile_sidebar_fill_color": "F6F6F6",
        "profile_text_color": "333333",
        "profile_use_background_image": true,
        "has_extended_profile": false,
        "default_profile": false,
        "default_profile_image": false,
        "following": false,
        "follow_request_sent": false,
        "notifications": false
    }
}*/