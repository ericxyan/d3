angular.module('myServices', [])
	.factory('socket', function ($rootScope) {
		return {
			on: function (eventName, callback) {
				var socket = io.connect('http://localhost:3000');
				socket.on(eventName, function () {
					var args = arguments;
					$rootScope.$apply(function () {
						callback.apply(socket, args);
					});
				});
			},
			emit: function (eventName, callback) {
				var socket = io.connect('http://localhost:3000');
				socket.on(eventName, function () {
					var args = arguments;
					$rootScope.$apply(function () {
						callback.apply(socket, args);
					});
				});
			}
		};
	});