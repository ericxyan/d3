angular.module('myConfig', ['ngRoute'])
.config(['$routeProvider', function ($routeProvider) {
	$routeProvider.
		when('/', {
			templateUrl: '/templates/twitter.html',
			controller: 'appCtrl'
		}).
		otherwise({
			redirectTo: '/'
		});
}]);