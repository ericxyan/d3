angular.module('myControllers', ['ngRoute', 'ui.bootstrap', 'myServices'])
    .controller('appCtrl', function($scope, $http, $filter, $log, socket) {
        /**
         * formate a date
         * @param  {string} date selected from date picker.
         * @return {string}      date in 'YYYY-MM-dd' format.
         */
        var formatDate = function(date) {
            return $filter('date')(date, 'yyyy-MM-dd'); // change date to 'YYY-MM-dd'.
        };

        $scope.getHTML = function(tweetId) {
            $http.get('/api/statuses/oembed/' + tweetId).success(function(res) {
                $log.log(res);
                return res.html;
            });
        }

        /**
         * Search for a district
         */
        $scope.searchDistrict = function(district) {
            $http.get('/api/district/' + district).success(function(res) {
                $scope.tweets = res;
            });
        }

        /**
         * Search for keyword
         */
        $scope.searchKeyword = function(keyword, startDate, geo) {
            $scope.tweets = [];
            // If date is not set, set default value as today.
            var since = typeof(startDate) == 'undefined' ? formatDate(new Date()) : formatDate(new Date(startDate));
            // construct api query parameters.
            var params = {
                'keyword': keyword,
                'since': since,
                'geocode': geo,  // Toronto: 43.6532260,-79.3831840,30km; home: 43.8134410,-79.3269270,1km
                'count': 100
            };
            // query api.
            $http.post('/api/keyword', params).success(function(res) {
                $scope.result = res.search_metadata;
                $scope.tweets = res.statuses;
            });
        };

        /**
         * start to listen to a stream.
         */
        // Toronto: -79.67,43.50,-79.10,43.91
        $scope.streamGeo = ''; // '-122.75,36.8,-121.75,37.8'; San Francisco
        $scope.searchStream = function() {
            $scope.tweets = [];
            var params = {
                'track': $scope.streamKeywords,
                'locations': $scope.streamGeo
            };
            // socket listens to stream
            socket.on('tweets', function(res) {
                $scope.tweets.push(res);
            });
            // request stream
            $http.post('/api/stream/statuses/filter', params).success(function(res) {});
        };

        /**
         * favorite a user
         * @param  {string} screenName target's name
         */
        $scope.follow = function(screenName) {
            $http.post('/api/follow/' + screenName).success(function(res) {
                alert('Followed' + screenName);
            });
        };

        /**
         * reply to a tweet
         * @param  {string} replyAuthor  the author's screen name.
         * @param  {string} replyText    content of reply.
         * @param  {string} replyTweetId target tweet's id.
         * @return {tweet}  res          replied tweet.
         */
        $scope.reply = function(replyAuthor, replyText, replyTweetId) {
            // construct params
            var params = {
                'replyAuthor': replyAuthor,
                'replyText': replyText,
                'replyTweetId': replyTweetId
            };

            $http.post('/api/reply', params).success(function(res) {
                alert('Reply succeed!');
            });
        };

        /**
         * send a direct message to a target user
         * @param  {string} screenName target's screen name
         * @param  {string} msg        text to be sent
         * @return {obj}    res        the sent message
         */
        $scope.sendMessage = function(screenName, msg) {
            var params = {
                'screenName': screenName,
                'msg': msg
            };

            $http.post('/api/directMessage', params).success(function(res) {
                alert('Succeed!');
            });
        }

    });