var express = require('express');
var router = express.Router();
var Twit = require('twit');
var db = require('../dbSchema');
// config
var T = new Twit({
    "consumer_secret": "VIUQQfUnHCnm6qfNhaoWrBq6u9ZMt69u2sWSs0TPMqB0JSZi43",
    "consumer_key": "FvZlW8g6WXj0o9HPkSmHGcqjj",
    "access_token": "3471550887-jKDi6g4wRxPEv8t3Y5wi1xYgUnGMr0R5UhGCJNg",
    "access_token_secret": "MpJO5geImldTAKJZm4YZfjlbXNrMXCx0UVOHZFjHzPeXr"
});
// db model
var Candidate = db.Candidate;
var Reply = db.Reply;

module.exports = function(io) {

    // get candidates from a district
    router.get('/district/:district', function(req, res, next) {
        Candidate.find({
            'district': req.params.district
        }, function(err, docs) {
            if (err) {
                res.send('Find district candidates failed!');
            } else {
                console.log('district: ' + req.params.district)
                console.log(docs);
                res.send(docs);
            }
        });
    });

    // listen to a stream
    router.post('/stream/statuses/filter', function(req, res, next) {
        console.log(req.body);
        var stream = T.stream('statuses/filter', req.body);

        stream.on('connect', function(req) {
            console.log('Connected to Twitter API.');
        });

        stream.on('disconnect', function(msg) {
            console.log('Disconnected from Twitter API. Message: ' + msg);
        });

        stream.on('reconnect', function(req, res, interval) {
            console.log('Trying to reconnect to Twitter API in ' + interval + ' ms.');
        });

        stream.on('tweet', function(tweet) {
            //console.log(tweet);
            
            if(tweet.place){
                console.log(tweet.place);
                console.log("new tweet coming!");
            } 
            io.sockets.emit('tweets', tweet);
        });
    });

    // get HTML snippet of a tweet
    router.get('/statuses/oembed/:tweetId', function(req, res, next) {
        T.get('statuses/oembed', {
            id: req.params.tweetId
        }, function(err, data, response) {
            if (err) {
                console.log(err);
            } else {
                res.send(data);
            }
        })
    });

    // search history tweets by @username, #tag, keywords
    router.post('/keyword', function(req, res, next) {
        //console.log(req.body);

        var qString = '';
        qString += req.body.keyword;
        //qString += ' since:' + req.body.since;
        // if geocode is not defined 
        if (req.body.geocode && req.body.geocode !== '') {
            qString += ' geocode:' + req.body.geocode
        };

        console.log(qString)

        T.get('search/tweets', {
            q: qString,
            count: req.body.count
        }, function(err, data, response) {
            res.send(data);
        })
    });

    // follow a user by screen_name, if follow=true, notify target user.
    router.post('/follow/:screenName', function(req, res, next) {
        console.log(req.params.screenName);

        T.post('friendships/create', {
            screen_name: req.params.screenName,
            follow: true
        }, function(err, data, response) {
            // returns the befriended user
            res.send(data);
            console.log(data);
        });
    });

    // reply to an existing tweet
    router.post('/reply', function(req, res, next) {
        console.log(req.body);

        T.post('statuses/update', {
            status: '@' + req.body.replyAuthor + ' ' + req.body.replyText,
            in_reply_to_status_id: req.body.replyTweetId
        }, function(err, data, response) {
            console.log(data);
            res.send(data);
        });
    });

    // send a direct message to a target user.
    router.post('/directMessage', function(req, res, next) {
        console.log(req.body);

        T.post('direct_messages/new', {
            screen_name: req.params.screenName,
            text: req.params.msg
        }, function(err, data, response) {
            console.log(data);
            res.send(data);
        });
    });

    return router;
}