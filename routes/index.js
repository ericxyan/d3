var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('templates/index', { title: 'Express' });
});

/* Pie chart */
/* url: http://bl.ocks.org/dbuezas/9572040 */
router.get('/piechart', function(req,res,next) {
	res.render('templates/pieChart');
});

router.get('/collapsible', function(req,res,next) {
	res.render('templates/collapsible');
});

router.get('/imagenodes', function(req,res,next) {
	res.render('templates/imageNodes');
});

router.get('/twitter/', function(req,res,next) {
	res.render('templates/twitter');
});

router.get('/twitterbox', function(req,res,next) {
	res.render('templates/twitterBox');
})
module.exports = router;
